import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('firewall_poc')


def test_directory_exists(host):
    assert host.file("/var/www/test").exists


def test_nginx_access_http(host):
    cmd = host.run('curl -f http://localhost/eicar.com')
    assert cmd.rc == 0


def test_nginx_access_https(host):
    cmd = host.run('curl -f https://localhost/eicar.com')
    assert cmd.rc == 0


def test_nginx_running_and_enabled(host):
    nginx = host.service("nginx")
    assert nginx.is_running
    assert nginx.is_enabled


def test_malware_folder_exists(host):
    assert host.file("/etc/malware").exists
